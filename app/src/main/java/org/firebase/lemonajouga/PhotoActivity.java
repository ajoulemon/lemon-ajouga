package org.firebase.lemonajouga;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class PhotoActivity extends AppCompatActivity {

    private static final int PICK_FROM_CAMERA = 0;
    private static final int PICK_FROM_ALBUM = 1;
    private static final int CROP_FROM_iMAGE = 2;

    private Uri mImageCaptureUri;
    private ImageView iv_UserPhoto;
    private int id_view;
    private String absoultePath;
    private StorageReference mStorageRef;
    private StorageTask uploadTask;
    private Task downloadUrl;
    String photo;
    private Button Next;
    private String generatedFilePath;
    private String LOG = "Sys";
    AlertDialog.Builder alert_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        FirebaseApp.initializeApp(this);
        iv_UserPhoto = (ImageView) findViewById(R.id.photo);

        mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com");


        Next = (Button) findViewById(R.id.Next);
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != photo) {
                    Intent intent = new Intent(getApplicationContext()
                            , ProfileActivity.class);
                    intent.putExtra("photo", photo);
                    startActivity(intent);
                } else {
                    alert_confirm = new AlertDialog.Builder(PhotoActivity.this);
                    alert_confirm.setMessage("사진을 업로드해 주세요").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();

                    // arlet 띄우기
                }

            }
        });
    }

    public void onButton1Click(View v) {
        id_view = v.getId();
        if (v.getId() == R.id.btn_UploadPicture) {

//            DialogInterface.OnClickListener cameraListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    doTakePhotoAction();
//                }
//            };

            DialogInterface.OnClickListener albumListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    doTakeAlbumAction();
                }
            };


            DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };

            new AlertDialog.Builder(this)
                    .setTitle("Choose an Image")
                    .setPositiveButton("Choose picture", albumListener)
                    .setNegativeButton("Cancel", cancelListener).show();


        }
    }


//    public void doTakePhotoAction() {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
//        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
//
//
////        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//        startActivityForResult(intent, PICK_FROM_CAMERA);
//    }

    public void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case PICK_FROM_ALBUM: {
                mImageCaptureUri = data.getData();
                Log.d("SmartWheel", mImageCaptureUri.getPath().toString());

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");
                intent.putExtra("crop", "true");
                intent.putExtra("outputX", 200);
                intent.putExtra("outputY", 200);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);
                intent.putExtra("circleCrop", true);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, CROP_FROM_iMAGE);

                final Bundle extras = data.getExtras();

                String filePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        "/ajouga/Profile/Images/" + System.currentTimeMillis() + ".jpg";

                if (mImageCaptureUri != null) {

//                    extras.putParcelable(MediaStore.EXTRA_OUTPUT, mIm9ageCaptureUri);
                }

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");
                    iv_UserPhoto.setImageBitmap(photo);



                    absoultePath = filePath;
                    break;
                }

            }
            case CROP_FROM_iMAGE: {
                if (resultCode != RESULT_OK) {
                    return;
                }




//                File f = new File(mImageCaptureUri.getPath());
//                if (f.exists()) {
//                    f.delete();
//                }




                iv_UserPhoto.setImageResource(0);
                iv_UserPhoto.setImageURI(data.getData());

                break;
            }

        }
        uploadFile();
    }


    private void uploadFile() {
        if (mImageCaptureUri != null) {

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("upload");
            progressDialog.show();

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            String userId = user.getUid();

            StorageReference riversRef = mStorageRef.child("images/profile").child(userId);

            uploadTask = riversRef.putFile(mImageCaptureUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "File Upload", Toast.LENGTH_LONG).show();

                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage(((int) progress) + "%upload...");

                        }
                    });


            downloadUrl =  riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {


                    photo = uri.toString();
                    // Got the download URL for 'users/me/profile.png'
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors

                }
            });

//            Intent intent = new Intent(getApplicationContext()
//                    , UserRoomDetail.class);
////<<<<<<< HEAD
//            intent.putExtra("downloadUrl", downloadUrl);



        } else {

        }

    }

//    public class MyApplication extends Application {
//        @Override
//        public void onCreate() {
//            super.onCreate();
//            SomeSdk.init(this);  // init some SDK, MyApplication is the Context
//        }
//    }


    private void storeCropImage(Bitmap bitmap, String filePath) {
        String dirPath =
                Environment.getExternalStorageDirectory().getAbsolutePath() + "/SmartWheel";
        File directory_SmartWheel = new File(dirPath);

        if (!directory_SmartWheel.exists())
            directory_SmartWheel.mkdir();

        File copyFile = new File(filePath);
        BufferedOutputStream out = null;

        try {

            copyFile.createNewFile();
            out = new BufferedOutputStream(new FileOutputStream(copyFile));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(copyFile)));

            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

//        public void doTakePhotoAction(){
//            Intent intent = new intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        String url = "tmp_" + String.valueOf()
//    }
//    }
//    }

