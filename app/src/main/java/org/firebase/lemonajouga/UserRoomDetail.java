package org.firebase.lemonajouga;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.makeramen.roundedimageview.RoundedImageView;

import org.firebase.lemonajouga.models.Party;
import org.firebase.lemonajouga.models.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//import static org.androidtown.kookjenamnyeo.R.id.imageView3;

public class UserRoomDetail extends AppCompatActivity {

    AlertDialog.Builder alert_confirm;
//    RoundedImageView roundedImageView;

//    Dialog dialog = new Dialog(UserRoomDetail.this);

    private FirebaseStorage storage;
    private FirebaseDatabase mDatabase;
    private StorageReference storageRef;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    ImageView userPhoto;
    private ImageView bottleFirst, bottleSecond, bottleThird, bottleFourth, bottleFifth;

    TextView dia_name, dia_gender, dia_country, dia_department;
    FirebaseUser user;
    private ImageView placeImage;
    private TextView placeTextUser;

    RelativeLayout join_btn;
    RelativeLayout disjoin_btn;
    RoundedImageView roundedImageView;

    String pid;
    String userId;
    String location;

    String nm, photoURI, gender, country, department;
    String participant;

    double kk = 0;
    int capacity = 0;

    boolean joined = false;

    List<String> participantList = new ArrayList<>();
    List<String> partyList = new ArrayList<>();
    List<String> join_partyList = new ArrayList<>();
    List<User> testData;

    Party party;
    final Context context = this;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext()
                , PartyListActivity.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_room_detail);



        TextView hashTagsTextFirst = (TextView) findViewById(R.id.hashTagsTextFirst);
        TextView dateText = (TextView) findViewById(R.id.dateText);
        TextView titleText = (TextView) findViewById(R.id.textTitle);
        placeTextUser = (TextView) findViewById(R.id.placeTextUser);
        placeImage = (ImageView) findViewById(R.id.placeImage);


        alert_confirm = new AlertDialog.Builder(UserRoomDetail.this);

        FirebaseApp.initializeApp(this);

        mDatabase = FirebaseDatabase.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        Intent intent = getIntent();

        party = (Party) getIntent().getSerializableExtra("party");
        pid = party.getId();

        location = party.getLocation();

        bottleFirst = (ImageView) findViewById(R.id.bottleFirst);
        bottleSecond = (ImageView) findViewById(R.id.bottleSecond);
        bottleThird = (ImageView) findViewById(R.id.bottleThird);
        bottleFourth = (ImageView) findViewById(R.id.bottleFourth);
        bottleFifth = (ImageView) findViewById(R.id.bottleFifth);



        mDatabase.getReference("users").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                HashMap<String, Object> d = (HashMap) dataSnapshot.getValue();

                if (null == d.get("parties") ) {
                    partyList.add(pid);
                } else {
                    partyList.addAll((Collection<? extends String>) d.get("parties"));
                    partyList.add(pid);

                    join_partyList.addAll((Collection<? extends String>) d.get("parties"));

                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        placeTextUser.setText(location);

        switch (location){
            case "성호관 테라스(Seongho hole Terrace)":{
                placeImage.setImageResource(R.drawable.seongho);
                break;
            }
            case "국제학사(International dorm)":{
                placeImage.setImageResource(R.drawable.kookje);
                break;
            }

            case "광교관(Gwanggyo dorm)":{
                placeImage.setImageResource(R.drawable.gwanggyo);
                break;
            }

            case "다산관(Dasan hole)":{
                placeImage.setImageResource(R.drawable.dasan);
                break;
            }

            case "원천관(Woncheon hole)":{
                placeImage.setImageResource(R.drawable.gwanggyo);
                break;
            }



        }



        for (int i = 0; i < party.getParticipant().size(); i++) {
            participant = party.getParticipant().get(i).toString();

            participantList.add(participant);
//
//            mDatabase.getReference("users").child(participant).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    HashMap<String, Object> d = (HashMap) dataSnapshot.getValue();
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });

            final int finalI = i;
            mDatabase.getReference("users").child(participant).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> d = (HashMap) dataSnapshot.getValue();

                    capacity = Integer.parseInt(d.get("capacity").toString());

                    nm = d.get("nickname").toString();
                    photoURI = d.get("photo").toString();
                    gender = d.get("gender").toString();
                    country = d.get("country").toString();
                    department = d.get("department").toString();


                    testData.add(new User(nm, photoURI, gender, country, department));

                    kk = capacity + kk;

                    if(finalI == party.getParticipant().size()-1) {
                        kk = kk / 2;
                        kk = kk / party.getParticipant().size();

                        if (kk <= 0.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_empty);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 1.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_half);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 1.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 2.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_half);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 2.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 3.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_half);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 3.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 4.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_half);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 4.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 5.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_half);
                        } else {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_full);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.userRoomDetailRecyclerView);
        testData = new ArrayList<>();



//        party.getParticipant();

        for (int i = 0; i < party.getParticipant().size(); i++) {
            participant = party.getParticipant().get(i).toString();

//            participantList.add(participant);

        }


//        testData.add(new User("슈발", "http://i.imgur.com/DvpvklR.png"));
//        testData.add(new User("슈발", "http://i.imgur.com/DvpvklR.png"));
//        testData.add(new User("슈발", "http://i.imgur.com/DvpvklR.png"));



        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        Adapter adapter = new Adapter(UserRoomDetail.this, testData);
        recyclerView.setAdapter(adapter);

//        roundedImageView = (RoundedImageView) findViewById(R.id.profileImage);
//        roundedImageView.setOnClickListener(new View.OnClickListener(){
//            final RelativeLayout userLayout = (RelativeLayout) findViewById(R.id.activity_profile_dialog_user);
//            @Override
//            public void onClick(View v) {
//                userLayout.setVisibility(View.VISIBLE);
//            }
//
//        });



//        recyclerView.setHasFixedSize(true);

        storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com/");
// Create a reference with an initial file path and name
//        StorageReference pathReference = storageRef.child(“images/profile.jpg”);
// Create a reference to a file from a Google Cloud Storage URI
        StorageReference gsReference = storage.getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com/images/profile.jpg");
// Create a reference from an HTTPS URL
// Note that in the URL, characters are URL escaped!
//        StorageReference httpsReference = storage.getReferenceFromUrl(“https://firebasestorage.googleapis.com/b/kookje-namnyeo-android.appspot.com/o/images%profile.jpg”);


        /**
         //        //TODO : user 가져와서
         //        if (null != adapter) {
         //            adapter.setDatas(여기다가 리스트 넣으면됨 List<USER>);
         }
         */
//        mDatabase.getReference("users").child(userId).addListenerForSingleValueEvent(
//                new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        // Get user value
//                        User user = dataSnapshot.getValue(User.class);
//                        String nickName = user.getNickname();
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        Log.w("test", "getUser:onCancelled", databaseError.toException());
//                    }
//                });

        findViewById(R.id.arrow_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext()
                        , PartyListActivity.class);
                startActivity(intent);
            }
        });

        join_btn = (RelativeLayout) findViewById(R.id.join);
        disjoin_btn = (RelativeLayout) findViewById(R.id.disjoin);

        join_btn.setOnClickListener(new View.OnClickListener() {
//            int count = 0;
            @Override
            public void onClick(View v) {

                alert_confirm.setMessage("이 Party에 참가하시겠습니까?").setCancelable(false).setPositiveButton("예",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (party.getMax() > party.getParticipant().size() || party.getMax() == 1) {

                                    join_btn.setVisibility(View.GONE);
                                    disjoin_btn.setVisibility(View.VISIBLE);

                                    participantList.add(userId);
                                    party.setParticipant(participantList);

                                    mDatabase.getReference("parties").child(pid).updateChildren(party.toMap());
                                    mDatabase.getReference("users").child(userId).child("parties").setValue(partyList);

                                    Toast.makeText(UserRoomDetail.this, "축하합니다! Party에 참가하셨습니다.", Toast.LENGTH_SHORT).show();

                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);

//                                    Intent intent = getIntent();
//                                    overridePendingTransition(0, 0);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                                    finish();
//                                    overridePendingTransition(0, 0);
//                                    startActivity(intent);

                                    intent.putExtra("party", party);

                                } else {
                                    Toast.makeText(UserRoomDetail.this, "제한 수를 초과한 방입니다.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).setNegativeButton("아니요",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();


            }
        });

        disjoin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert_confirm.setMessage("이 Party에서 나가시겠습니까?").setCancelable(false).setPositiveButton("예",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                join_btn.setVisibility(View.VISIBLE);
                                disjoin_btn.setVisibility(View.GONE);

                                for(int i = 0; i < participantList.size(); i++) {
                                    if (participantList.get(i).equals(userId)) {
                                        participantList.remove(i);
                                    }
                                }

                                for (int i = 0; i < join_partyList.size(); i++) {
                                    if (join_partyList.get(i).equals(pid)) {
                                        join_partyList.remove(i);
                                    }
                                }

                                party.setParticipant(participantList);

//                                mDatabase.getReference("parties").child(pid).child("participant").setValue(null);
                                mDatabase.getReference("parties").child(pid).updateChildren(party.toMap());
                                mDatabase.getReference("users").child(userId).child("parties").setValue(join_partyList);

                                Toast.makeText(UserRoomDetail.this, "다른 Party에서 봐요~", Toast.LENGTH_SHORT).show();
                                finish();
                                Intent intent = new Intent(getApplicationContext()
                                        , UserRoomDetail.class);
                                intent.putExtra("party", party);
                                startActivity(intent);

                            }
                        }).setNegativeButton("아니요",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });

        for(int i = 0; i < participantList.size(); ++i) {
            if (participantList.get(i).equals(userId)) {
                joined = true;
            }
        }

        if (joined) { // 방에 참가한 유저라면
            join_btn.setVisibility(View.GONE);
            disjoin_btn.setVisibility(View.VISIBLE);
        } else { // 처음 들어온 유저라면
            join_btn.setVisibility(View.VISIBLE);
            disjoin_btn.setVisibility(View.GONE);
        }

        String titleString = party.getTitle();
        titleText.setText(titleString);

        String hashTagsStringFirst = party.getHashtags();
        hashTagsTextFirst.setText(hashTagsStringFirst);

//        Long dateLong = party.getDate();
        Long millisecond = party.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
        String dateLong = formatter.format(new Date(millisecond));
        dateText.setText(dateLong);//        dateText.setText(getDurationBreakdown(dateLong));

        party.getParticipant();

        TextView participateNum = (TextView) findViewById(R.id.maxText);
        String merge = party.getParticipant().size() + "/" + party.getMax();
        participateNum.setText(merge);
        if (party.getMax() == 1) {
            participateNum.setText(party.getParticipant().size() + "/∞");
        }
        Button button2 = (Button) findViewById(R.id.closebuttonuser);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout r = (RelativeLayout) findViewById(R.id.relativelayouts);
                r.setVisibility(View.GONE);
            }
        });

        StorageReference pathReference = storageRef.child("images").child("profile").child(party.getHostId());

        userPhoto = (ImageView) findViewById(R.id.userPhoto);


        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(pathReference)
                .into(userPhoto);


        ImageView userPhoto2 = (ImageView) findViewById(R.id.userPhoto2);

        StorageReference pathReference2 = storageRef.child("images").child("profile").child(party.getHostId());

        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(pathReference2)
                .into(userPhoto2);

        dia_name = (TextView) findViewById(R.id.dia_name);
        dia_gender = (TextView) findViewById(R.id.dia_gender);
        dia_country = (TextView) findViewById(R.id.dia_country);
        dia_department = (TextView) findViewById(R.id.dia_department);



    }

    public void setViz(int pos, User user) {

        String p = party.getParticipant().get(pos).toString();

        dia_name.setText(user.getNickname());
        dia_gender.setText(user.getGender());
        dia_country.setText(user.getCountry());
        dia_department.setText(user.getDepartment());

        RelativeLayout r = (RelativeLayout) findViewById(R.id.relativelayouts);

        r.setVisibility(View.VISIBLE);

//        r.bringToFront();


    }


}