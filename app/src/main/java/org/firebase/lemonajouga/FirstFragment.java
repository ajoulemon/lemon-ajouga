package org.firebase.lemonajouga;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.firebase.lemonajouga.models.Party;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FirstFragment extends Fragment {

    private FirebaseDatabase mDatabase;
    private FirebaseUser user;

    private Party party;

    private TextView nickName;
    private TextView title;
    private TextView hashtag;
    private TextView participateNum;
    private ImageView profileImage;
    private TextView country;
    private FirebaseStorage storage;
    private ImageView maxView;

    public FirstFragment(){
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e("pagerFragment", "" + getArguments().getInt("position"));
        party = (Party) getArguments().get("party");
//        user = (User) getArguments().get("user");

        super.onCreate(savedInstanceState);

//        maxView = (ImageView) getView().findViewById(R.id.max);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.activity_first_fragment, container, false);
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //    TextView textView = (TextView) view.findViewById(R.id.testText);
        //    textView.setText(getArguments().getInt("position")+"번째 페이지");

        RelativeLayout viewpagerLayout = (RelativeLayout) view.findViewById(R.id.viewpagerLayout);

        mDatabase = FirebaseDatabase.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String userId = user.getUid();


        viewpagerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (party.getHostId().equals(userId)) {

                    Intent intent = new Intent(getApplicationContext()
                            , HostRoomDetail.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("party", party);
//                    intent.putExtra("pid", party.getId())
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(getApplicationContext()
                            , UserRoomDetail.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("party", party);
                    startActivity(intent);
                }
            }
        });

        title = (TextView) view.findViewById(R.id.title);
        title.setText(party.getTitle());

        nickName = (TextView) view.findViewById(R.id.nickName);
        nickName.setText(party.getHostNickname());

        hashtag = (TextView) view.findViewById(R.id.hashTag);
        hashtag.setText(party.getHashtags());

        participateNum = (TextView) view.findViewById(R.id.participateNum);
        String merge = party.getParticipant().size() + "/" + party.getMax();
        participateNum.setText(merge);

        if (party.getMax() == 1){
            participateNum.setText(party.getParticipant().size() + "/∞");
        }

//        profileImage = (profileImage) view.findViewById(R.id.profileImage);
//        profileImage.setImageResource(party.getHostId());

        country = (TextView) view.findViewById(R.id.country);
        country.setText(party.getHostCountry());

        storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com/");
        StorageReference pathReference = storageRef.child("images").child("profile").child(party.getHostId());
        profileImage = (ImageView) getView().findViewById(R.id.profileImage);
        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(pathReference)
                .into(profileImage);




    }
}