package org.firebase.lemonajouga;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.firebase.lemonajouga.models.User;

public class InfoActivity extends AppCompatActivity {
    private Spinner spinMajor;
    private Button prevButton2;
    private Button nextButton2;
    private EditText contactNumber;
//    private int capacity, bottleId;
    private Button bottleFirst;
    private Button bottleSecond;
    private Button bottleThird;
    private Button bottleFourth;
    private Button bottleFifth;
//    private Boolean isPressed;

    int clickcount=0;
    int checkcap;

    private DatabaseReference mDatabase;


    AlertDialog.Builder alert_confirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        mDatabase = FirebaseDatabase.getInstance().getReference();


        spinMajor = (Spinner) findViewById(R.id.spinMajor);
        prevButton2 = (Button) findViewById(R.id.prevButton2);
        nextButton2 = (Button) findViewById(R.id.nextButton2);
        contactNumber = (EditText) findViewById(R.id.contact);

        bottleFirst = (Button) findViewById(R.id.bottleFirst);
        bottleSecond = (Button) findViewById(R.id.bottleSecond);
        bottleThird = (Button) findViewById(R.id.bottleThird);
        bottleFourth = (Button) findViewById(R.id.bottleFourth);
        bottleFifth = (Button) findViewById(R.id.bottleFifth);

//        capacity = (int) findViewById(R.id.capacity);

        bottleFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount = clickcount + 1;
                if(clickcount == 0) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 0;
                }
                else if(clickcount==1) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_half);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 1;
                }
                else if(clickcount==2) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 2;
                }
                else {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    clickcount = 0;
                    checkcap = 0;
                }
//                Toast.makeText(getApplicationContext(),"Button clicked count is "+clickcount, Toast.LENGTH_LONG).show();
            }
        });

        bottleSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount= clickcount + 1;
                if(clickcount == 0) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 2;
                }
                else if(clickcount==1) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_half);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 3;
                }
                else if(clickcount==2) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 4;
                }
                else {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    clickcount = 0;
                    checkcap = 2;
                }
//                Toast.makeText(getApplicationContext(),"Button clicked count is "+clickcount, Toast.LENGTH_LONG).show();
            }
        });

        bottleThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount= clickcount + 1;
                if(clickcount == 0) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 4;
                }
                else if(clickcount==1) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_half);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 5;
                }
                else if(clickcount==2) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 6;
                }
                else {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    clickcount = 0;
                    checkcap = 4;
                }
//                Toast.makeText(getApplicationContext(),"Button clicked count is "+clickcount, Toast.LENGTH_LONG).show();
            }
        });

        bottleFourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount= clickcount + 1;
                if(clickcount == 0) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 6;
                }
                else if(clickcount==1) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_half);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 7;
                }
                else if(clickcount==2) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 8;
                }
                else {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    clickcount = 0;
                    checkcap = 6;
                }
//                Toast.makeText(getApplicationContext(),"Button clicked count is "+clickcount, Toast.LENGTH_LONG).show();
            }
        });

        bottleFifth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount= clickcount + 1;
                if(clickcount == 0) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    checkcap = 8;
                }
                else if(clickcount==1) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_half);
                    checkcap = 9;
                }
                else if(clickcount==2) {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_full);
                    checkcap = 10;
                }
                else {
                    bottleFirst.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleSecond.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleThird.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFourth.setBackgroundResource(R.drawable.ic_bottle_full);
                    bottleFifth.setBackgroundResource(R.drawable.ic_bottle_empty);
                    clickcount = 0;
                    checkcap = 8;
                }
//                Toast.makeText(getApplicationContext(),"Button clicked count is "+clickcount, Toast.LENGTH_LONG).show();
            }
        });



        prevButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext()
                        , ProfileActivity.class);
                startActivity(intent);
            }
        });

        nextButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String uid = getIntent().getStringExtra("uid");
                String nickname = getIntent().getStringExtra("nickname");
                String country = getIntent().getStringExtra("country");

                String gender = getIntent().getStringExtra("gender");

                String department = spinMajor.getSelectedItem().toString();
                String contact = contactNumber.getText().toString();
                int capacity = checkcap;
                String photo = getIntent().getStringExtra("photo");

                if (isValidate(contact)) {
                    User appUser = new User(uid, nickname, country, gender, department, contact, capacity, photo);


                    mDatabase.child("users").child(uid).setValue(appUser.toMap());



                    Intent intent = new Intent(getApplicationContext()
                            , LoginCompleteActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    alert_confirm = new AlertDialog.Builder(InfoActivity.this);
                    alert_confirm.setMessage("정보를 모두 입력해주세요").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();

                    // arlet 띄우기
                }
            }
        });




        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.ajouCollege));

//        getCapacity();


    }

    public boolean isValidate(String s) {
        boolean check = s.length() != 0;

        return check;
    }


//    private getCapacity(){
//
//        final boolean isPressed = false;
//
//        bottleFirst.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//            @Override
//            public void onClick(View v) {
//                if (isPressed)
//                    bottleFirst.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleId = bottleFirst.getId();
//            }
//        });
//
//        bottleSecond.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isPressed)
//                    bottleFirst.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleSecond.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleId = bottleSecond.getId();
//            }
//        });
//
//        bottleSecond.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isPressed)
//                    bottleFirst.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleSecond.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleThird.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleId = bottleThird.getId();
//            }
//        });
//
//        bottleSecond.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isPressed)
//                    bottleFirst.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleSecond.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleThird.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleFourth.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleId = bottleFourth.getId();
//            }
//        });
//
//        bottleSecond.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isPressed)
//                    bottleFirst.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleSecond.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleThird.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleFourth.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleFifth.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_bottle_full));
//                bottleId = bottleFifth.getId();
//            }
//
//
//        });
//
//        if(bottleId == R.id.bottleFirst){
//            capacity = 1;
//        }
//
//        else if(bottleId == R.id.bottleSecond){
//            capacity = 2;
//        }
//
//        else if(bottleId == R.id.bottleThird){
//            capacity = 3;
//        }
//
//        else if(bottleId == R.id.bottleFourth){
//            capacity = 4;
//        }
//
//        else {
//            capacity = 5;
//        }
//
//        return capacity;
//    }

}