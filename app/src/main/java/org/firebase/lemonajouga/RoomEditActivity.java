package org.firebase.lemonajouga;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import org.firebase.lemonajouga.models.Party;

public class RoomEditActivity extends AppCompatActivity {

    private static final String TAG = "RoomEdit";

    AlertDialog.Builder alert_confirm;

    private FirebaseDatabase mDatabase;
    private FirebaseAuth firebaseAuth;

    GregorianCalendar calendar;

    private HashTagHelper mTextHashTagHelper;
    private TextView mHashTagText;

    String max_spin;

    String pid;
    String title;
    String hashtags;
    String hostId;
    String placeName;
    Long date;
    int max;

    public String test;

    int year, month, day, hour, minute;
    int real_year, real_month, real_day, real_hour, real_minute;

    String am_pm;

    String userId;

    String nm;
    String cnt;

    Party parties;
    Party party;



//    private FirebaseDatabase database;
//    private DatabaseReference reference;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_edit);
        FirebaseApp.initializeApp(this);

        mDatabase = FirebaseDatabase.getInstance();

        alert_confirm = new AlertDialog.Builder(RoomEditActivity.this);

        // Hash Tag
        mHashTagText = (TextView) findViewById(R.id.editTag);
        mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.colorPrimary),
                new HashTagHelper.OnHashTagClickListener() {
                    @Override
                    public void onHashTagClicked(String hashTag) {
                        Log.d(TAG, hashTag);
                    }
                });

        // pass a TextView or any descendant of it (incliding EditText) here.
        // Hash tags that are in the text will be hightlighed with a color passed to HasTagHelper
        mTextHashTagHelper.handle(mHashTagText);

        test = mHashTagText.getText().toString();
        Log.d(TAG, test);

        party = (Party) getIntent().getSerializableExtra("party");


        //Picker

        calendar = new GregorianCalendar();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        findViewById(R.id.Date_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(RoomEditActivity.this, dateSetListener, year, month, day).show();

            }
        });

        findViewById(R.id.Time_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RoomEditActivity.this, timeSetListener, hour, minute, false).show();
            }
        });

        final EditText editTitle = (EditText) findViewById(R.id.editText);
        final Spinner spin_loc = (Spinner) findViewById(R.id.spinner_location);
        final Spinner spin_max = (Spinner) findViewById(R.id.spinner_max);
        final EditText editTag = (EditText) findViewById(R.id.editTag);


        findViewById(R.id.Edit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pid = party.getId(); //이부분은 현재 방 아이디 가져오는 걸로

                title = editTitle.getText().toString();
                hashtags = editTag.getText().toString();

                hostId = party.getHostId();

                calendar.set(real_year, real_month, real_day,real_hour,real_minute, 0);
                date = calendar.getTimeInMillis();

                max_spin = spin_max.getSelectedItem().toString();

                if (max_spin.equals("2명")) {
                    max = 2;
                } else if (max_spin.equals("3명")) {
                    max = 3;
                } else if (max_spin.equals("4명")) {
                    max = 4;
                } else if (max_spin.equals("5명")) {
                    max = 5;
                } else if (max_spin.equals("6명")) {
                    max = 6;
                } else if (max_spin.equals("7명")) {
                    max = 7;
                } else if (max_spin.equals("8명+")) {
                    max = 1;
                }

                if (isValidate(title) & isValidate(hashtags) & isValidate(date.toString())) {

                    alert_confirm.setMessage("이 정보로 방을 수정하시겠습니까?").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    parties = new Party(pid, title, hashtags, hostId, date, max, placeName);

                                    parties.setParticipant(party.getParticipant());

                                    nm = party.getHostNickname();
                                    cnt = party.getHostCountry();

                                    //
                                    parties.setHostNickname(nm);
                                    parties.setHostCountry(cnt);

                                    // parties에 정보 추가
                                    mDatabase.getReference().child("parties").child(pid).setValue(parties.toMap());


//                                    mDatabase.getReference().child("users").child(hostId).child("parties").setValue(pid);

                                    Toast.makeText(RoomEditActivity.this, "성공적으로 방이 수정되었습니다.", Toast.LENGTH_SHORT).show();
                                    finish();

                                }
                            }).setNegativeButton("취소",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();
                } else {
                    alert_confirm.setMessage("정보를 모두 입력해주세요").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();
                }


            }
        });

        findViewById(R.id.goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        findViewById(R.id.prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            real_year = year;
            real_month = monthOfYear;
            real_day = dayOfMonth;

            String msg = String.format("%d-%d-%d", year,monthOfYear+1, dayOfMonth);
            Toast.makeText(RoomEditActivity.this, msg, Toast.LENGTH_SHORT).show();

            TextView DTxt = (TextView) findViewById(R.id.Date_text);
            DTxt.setText(msg);


        }

    };

    TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // TODO Auto-generated method stub
            real_hour = hourOfDay;
            real_minute = minute;
            real_hour = hourOfDay;
            real_minute = minute;
            if (real_hour >= 12 & real_hour < 24) {
                am_pm = "PM";
                String msg = String.format("%s %d:%d", am_pm, hourOfDay-12, minute);
                Toast.makeText(RoomEditActivity.this, msg, Toast.LENGTH_SHORT).show();

                TextView TTxt = (TextView) findViewById(R.id.Time_text);
                TTxt.setText(msg);

            } else {
                am_pm = "AM";
                String msg = String.format("%s %d:%d", am_pm, hourOfDay, minute);
                Toast.makeText(RoomEditActivity.this, msg, Toast.LENGTH_SHORT).show();

                TextView TTxt = (TextView) findViewById(R.id.Time_text);
                TTxt.setText(msg);
            }
        }

    };

    public boolean isValidate(String s) {
        boolean check = s.length() != 0;

        return check;
    }
}
