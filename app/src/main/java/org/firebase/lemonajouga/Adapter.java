package org.firebase.lemonajouga;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import org.firebase.lemonajouga.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by woo on 2017. 5. 31..
 */

    public class Adapter extends RecyclerView.Adapter {
    private Context context;
    private List<User> datas;
    private RoundedImageView roundedImageView;
    int num;
    User user;
    View relativelayout;

//    private CustomDialog mCustomDialog;]

    public Adapter(Context context, List<User> datas) {
        this.context = context;
        this.datas = datas;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = null;
        RecyclerView.ViewHolder vh = null;
        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_room_host_profile, parent, false);
            vh = new HostViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_room_user_profile, parent, false);
            vh = new UserViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
//        dialog = new Dialog(context);
//        dialog.setContentView(R.layout.activity_profile_dialog_user);
//        RoundedImageView roundedImageview = roundedImageView.findViewById(R.id.profileImage);
//        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View v = vi.inflate(R.layout.activity_user_room_detail, null);
//        relativelayout = v.findViewById(R.id.relativelayouts);

        num = position;


        user = datas.get(position);
        if (holder instanceof HostViewHolder) {
            Glide.with(context)
                    .load(user.getPhoto())
                    .into((ImageView) (((HostViewHolder) holder).profileImage));
            ((HostViewHolder) holder).profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof UserRoomDetail){
                        ((UserRoomDetail)context).setViz(num, user);
                    } else if (context instanceof HostRoomDetail){
                        ((HostRoomDetail)context).setViz2(num, user);
                    }
//                    dialog.show();
//                    relativelayout.setVisibility(View.VISIBLE);
                    Log.d("Adapter","This is HostView");
                }
            });
            ((HostViewHolder) holder).nickName.setText(user.getNickname());

        } else {
            Glide.with(context)
                    .load(user.getPhoto())
                    .into(((UserViewHolder) holder).profileImage);
            ((UserViewHolder) holder).profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof UserRoomDetail){
                        ((UserRoomDetail)context).setViz(num, user);
                    } else if (context instanceof HostRoomDetail){
                        ((HostRoomDetail)context).setViz2(num, user);
                    }
                    Log.d("Adapter","This is UserView");
                }
            });
            ((UserViewHolder) holder).nickName.setText(user.getNickname());
        }
//        RoundedImageView = roundedImageView.findViewById(R.id.profileImage);
//        RelativeLayout relativelayout = (RelativeLayout) relativelayout.findViewById(R.id.relativelayouts);
//
//        roundedImageView.setOnClickListener(new View.OnClickListener(){
//
//            RoundedImageView roundedImageView = roundedImageView.findViewById(R.id.profileImage);
//            @Override
//            public void onClick(View v) {
//                relativelayout.setVisibility(View.VISIBLE);
//            }
//
//        });

    }




    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return position;
        }
    }

    public class HostViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView profileImage;
        public TextView nickName;

        public HostViewHolder(View v) {
            super(v);
            this.profileImage = (RoundedImageView) v.findViewById(R.id.hostProfileProfileImage);
            this.nickName = (TextView) v.findViewById(R.id.hostProfileNickName);
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView profileImage;
        public TextView nickName;

        public UserViewHolder(View v) {
            super(v);
            this.profileImage = (RoundedImageView) v.findViewById(R.id.userProfileProfileImage);
            this.nickName = (TextView) v.findViewById(R.id.userProfileNickName);
        }
    }

    public void setDatas(List<User> datas) {
        if (null != this.datas) {
            this.datas = datas;
        } else {
            this.datas = new ArrayList<>();
            this.datas = datas;
        }
        notifyDataSetChanged();
    }
}
