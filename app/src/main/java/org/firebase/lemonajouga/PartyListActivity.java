package org.firebase.lemonajouga;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.firebase.lemonajouga.models.Party;

import java.util.ArrayList;
import java.util.List;

public class PartyListActivity extends AppCompatActivity {

    String userId;

    private MyPagerAdapter adapter;
    private FirebaseDatabase mDatabase;

    boolean btnChange = true;
    private Button add;
    private Button myHome;
    private ViewPager vp;
    private List<FirstFragment> firstFragments;

    List<Party> partyList;
    List<Party> my_partyList;
    List<String> my_partyId;

    AlertDialog.Builder alert_confirm;


    @Override
    public void onBackPressed() {
        alert_confirm = new AlertDialog.Builder(PartyListActivity.this);
        alert_confirm.setMessage("서비스를 종료하시겠습니까?").setCancelable(false).setPositiveButton("예",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니요",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alert = alert_confirm.create();
        alert.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_list);

        String a = getIntent().getStringExtra("click_num");
        if (null != a) {
            if (a.equals("total")) {
                btnChange = true;
            } else if(a.equals("my")) {
                btnChange = false;
            }

        }


//        Bundle args = PartyListActivity.getIntent().getExtras();
//        boolean istrue= args.getBoolean(EXTRA_ANSWER_IS_TRUE, false);
//        if (btnChange) {
//            btnChange = getIntent().getBooleanExtra("btnbool", true);
//
//        }


//        FirstFragment firstFragment = new FirstFragment();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable();
//        firstFragment.setArguments(bundle);
        FirebaseApp.initializeApp(this);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        mDatabase = FirebaseDatabase.getInstance();

        my_partyId = new ArrayList<>();

        mDatabase.getReference("users").child(userId).child("parties").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot d : dataSnapshot.getChildren()) {

                    String a = d.getValue().toString();
                    my_partyId.add(a);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        vp = (ViewPager) findViewById(R.id.vp);

        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext()
                        , RoomMakeActivity.class);
                startActivity(intent);
            }
        });

        myHome = (Button) findViewById(R.id.myHome);
        myHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnChange = !btnChange;
//
//                vp.setPageMargin(30);
//                adapter = new MyPagerAdapter(getSupportFragmentManager());
//                vp.setAdapter(adapter);
                if (btnChange) {
                    finish();
                    Intent intent = new Intent(getApplicationContext()
                            , PartyListActivity.class);
//                    intent.putExtra("btnbool",btnChange);
                    intent.putExtra("click_num","total");
                    startActivity(intent);
                    myHome.setBackgroundResource(R.drawable.ic_home_white);


                } else {
                    finish();
                    Intent intent = new Intent(getApplicationContext()
                            , PartyListActivity.class);
                    intent.putExtra("click_num","my");
                    startActivity(intent);
                    myHome.setBackgroundResource(R.drawable.ic_home_pink);

                    Toast.makeText(getApplicationContext(),"It shows only the room where I participated.", Toast.LENGTH_LONG).show();
                }

            }

        });

//        myHome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                btnChange = !btnChange;
//
//                if (btnChange) {
//
//                } else {
//                    Snackbar.make(v, "나의 방에 추가된 방만 보여집니다.", 2500).
//                            setAction("취소", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                }
//                            }).show();
//                }
//            }
//
//        });

        if (btnChange) {

            vp.setPageMargin(30);
            adapter = new MyPagerAdapter(getSupportFragmentManager());
            vp.setAdapter(adapter);

            mDatabase.getReference("parties").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    partyList = new ArrayList<>();
//                        userList = new ArrayList<>();

                    for (DataSnapshot d : dataSnapshot.getChildren()) {

                        Party p = d.getValue(Party.class); // Parties 형태로 가져다 주세요
                        partyList.add(p);


//                            Toast.makeText(getApplicationContext(), " "+d.getValue(), Toast.LENGTH_SHORT).show();
                    }
                    firstFragments = new ArrayList<>();

                    for (int i = 0; i < partyList.size(); i++) {

                        FirstFragment fragment = new FirstFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("party", (Party) partyList.get(i));
                        bundle.putInt("position", i);
                        fragment.setArguments(bundle);
                        firstFragments.add(fragment);
                    }
                    if (null != adapter) {
                        adapter.setData(firstFragments);
                    }

                }


                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            myHome.setBackgroundResource(R.drawable.ic_home_pink);

            vp.setPageMargin(30);
            adapter = new MyPagerAdapter(getSupportFragmentManager());
            vp.setAdapter(adapter);

            mDatabase.getReference("parties").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    my_partyList = new ArrayList<>();
//                        userList = new ArrayList<>();

                    for (DataSnapshot d : dataSnapshot.getChildren()) {

                        Party p = d.getValue(Party.class); // Parties 형태로 가져다 주세요


                        for (int i = 0; i < p.getParticipant().size(); i++) {
                            if (p.getParticipant().get(i).equals(userId)) {
                                my_partyList.add(p);
                            }

                        }
                    }

                    firstFragments = new ArrayList<>();

                    for (int i = 0; i < my_partyList.size(); i++) {

                        FirstFragment fragment = new FirstFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("party", (Party) my_partyList.get(i));
                        bundle.putInt("position", i);
                        fragment.setArguments(bundle);
                        firstFragments.add(fragment);
                    }
                    if (null != adapter) {
                        adapter.setData(firstFragments);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            }
        }


    View.OnClickListener movePageListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int tag = (int) v.getTag();
            vp.setCurrentItem(tag);
        }
    };



    private class MyPagerAdapter extends FragmentStatePagerAdapter
    {
        private List<FirstFragment> fragments;
        public MyPagerAdapter(android.support.v4.app.FragmentManager fm)
        {
            super(fm);
        }
        @Override
        public FirstFragment getItem(int position)
        {
            if (null != fragments) {
                return fragments.get(position);
            } else {
                return null;
            }
        }
        @Override
        public int getCount()
        {
            if (null != fragments) {
                return fragments.size();
            } else {
                return 0;
            }
        }

        @Override
        public float getPageWidth(int position) {
            return super.getPageWidth(position)*0.8f;
        }

        public void setData(List<FirstFragment> data) {
            if (null == fragments) {
                fragments = new ArrayList<>();
                fragments = data;
            } else {
                fragments = data;
            }
            notifyDataSetChanged();
        }
    }

}