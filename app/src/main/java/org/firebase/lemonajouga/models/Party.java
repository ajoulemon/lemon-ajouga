package org.firebase.lemonajouga.models;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by KOHANSOL on 2017-05-13.
 */

public class Party implements Serializable {
    private String id;
    private String title;
    private String hashtags;
    private String hostId;
    private Long date;
    private int max;

    private String hostNickname;
    private String hostCountry;

    private List<String> participant;

    private String location;

    public Party() {

    }

    public Party(String id, String title, String hashtags, String hostId, Long date, int max, String location) {
        this.id = id;
        this.title = title;
        this.hashtags = hashtags;
        this.hostId = hostId;
        this.date = date;
        this.max = max;
        this.location = location;
    }



    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap();
        result.put("id", id);
        result.put("title", title);
        result.put("hashtags", hashtags);
        result.put("hostId", hostId);
        result.put("date", date);
        result.put("max", max);
        result.put("participant", participant);
        result.put("hostNickname", hostNickname);
        result.put("hostCountry", hostCountry);
        result.put("location", location);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getHostNickname() {
        return hostNickname;
    }

    public void setHostNickname(String hostNickname) {
        this.hostNickname = hostNickname;
    }

    public String getHostCountry() {
        return hostCountry;
    }

    public void setHostCountry(String hostCountry) {
        this.hostCountry = hostCountry;
    }

    public List<String> getParticipant() {
        return participant;
    }

    public void setParticipant(List<String> participant) {
        this.participant = participant;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
