package org.firebase.lemonajouga.models;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by KOHANSOL on 2017-05-13.
 */

public class User implements Serializable{
    private String uid;
    private String nickname;
    private String country;
    private String gender;

    private String department;

    private String contact;
    private int capacity;
    private String photo;
    private List<String> parties;

    public User() {

    }


    public User(String uid) {

    }

    public User(String nickname, String photo, String gender, String country, String department) {
        this.nickname = nickname;
        this.photo = photo;
        this.country = country;
        this.gender = gender;
        this.department = department;
    }

    public User(String uid, String nickname, String country, String gender, String department, String contact, int capacity, String photo) {
        this.uid = uid;
        this.nickname = nickname;
        this.country = country;
        this.gender = gender;
        this.department = department;
        this.contact = contact;
        this.capacity = capacity;
        this.photo = photo;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap();
        result.put("uid", uid);
        result.put("nickname", nickname);
        result.put("country", country);
        result.put("gender", gender);
        result.put("department", department);
        result.put("contact", contact);
        result.put("capacity", capacity);
        result.put("photo", photo);
        result.put("parties", parties);
        return result;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<String> getParties() {
        return parties;
    }

    public void setParties(List<String> parties) {
        this.parties = parties;
    }
}
