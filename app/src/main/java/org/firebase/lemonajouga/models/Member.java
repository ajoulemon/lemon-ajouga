package org.firebase.lemonajouga.models;

import java.util.List;

/**
 * Created by KOHANSOL on 2017-05-13.
 */

public class Member {
    private String id;
    private String name;
    private boolean isHost;
    private List<Party> joinedParties;

    public Member(String id, String name, boolean isHost) {
        this.id = id;
        this.name = name;
        this.isHost = isHost;
    }

    public Member(String name, boolean isHost) {
        this.name = name;
        this.isHost = isHost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setHost(boolean host) {
        isHost = host;
    }

    public List<Party> getJoinedParties() {
        return joinedParties;
    }

    public void setJoinedParties(List<Party> joinedParties) {
        this.joinedParties = joinedParties;
    }
}