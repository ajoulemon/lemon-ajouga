package org.firebase.lemonajouga;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.firebase.lemonajouga.models.Party;
import org.firebase.lemonajouga.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class HostRoomDetail extends AppCompatActivity {

    AlertDialog.Builder alert_confirm;

    private FirebaseStorage storage;
    private FirebaseDatabase mDatabase;
    private StorageReference storageRef;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private ImageView placeImage;
    private TextView placeTextHost;
    TextView dia_name, dia_gender, dia_country, dia_department;


    FirebaseUser user;
    String userId;
    String pid;
    String location;

    String nm, photoURI, userURI, gender, country, department;
    String participant;


    ImageView bottleFirst;
    ImageView bottleSecond;
    ImageView bottleThird;
    ImageView bottleFourth;
    ImageView bottleFifth;

    double kk = 0;
    int capacity = 0;

    List<User> testData;
    List<String> participantList = new ArrayList<>();

    private Party party;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext()
                , PartyListActivity.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_room_detail);
        FirebaseApp.initializeApp(this);

        mDatabase = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        party = (Party) getIntent().getSerializableExtra("party");
        pid = party.getId();

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        testData = new ArrayList<>();
        location = party.getLocation();

        placeImage = (ImageView) findViewById(R.id.placeImageHost);

//        placeHost = getIntent().getStringExtra("placeHost");

        placeTextHost = (TextView) findViewById(R.id.placeTextHost);




        bottleFirst = (ImageView) findViewById(R.id.bottleFirst);
        bottleSecond = (ImageView) findViewById(R.id.bottleSecond);
        bottleThird = (ImageView) findViewById(R.id.bottleThird);
        bottleFourth = (ImageView) findViewById(R.id.bottleFourth);
        bottleFifth = (ImageView) findViewById(R.id.bottleFifth);


        placeTextHost.setText(location);

        switch (location){
            case "성호관 테라스(Seongho hole Terrace)":{
                placeImage.setImageResource(R.drawable.seongho);
                break;
            }
            case "국제학사(International dorm)":{
                placeImage.setImageResource(R.drawable.kookje);
                break;
            }

            case "광교관(Gwanggyo dorm)":{
                placeImage.setImageResource(R.drawable.gwanggyo);
                break;
            }

            case "다산관(Dasan hole)":{
                placeImage.setImageResource(R.drawable.dasan);
                break;
            }

            case "원천관(Woncheon hole)":{
                placeImage.setImageResource(R.drawable.gwanggyo);
                break;
            }



        }



        for (int i = 0; i < party.getParticipant().size(); i++) {
            participant = party.getParticipant().get(i).toString();

            participantList.add(participant);

            final int finalI = i;
            mDatabase.getReference("users").child(participant).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, Object> d = (HashMap) dataSnapshot.getValue();

                    if (participant.equals(userId)) {
                        userURI = d.get("photo").toString();
                    }

                    nm = d.get("nickname").toString();
                    photoURI = d.get("photo").toString();
                    gender = d.get("gender").toString();
                    country = d.get("country").toString();
                    department = d.get("department").toString();


                    testData.add(new User(nm, photoURI, gender, country, department));


                    capacity = Integer.parseInt(d.get("capacity").toString());

                    kk = capacity + kk;

                    if(finalI == party.getParticipant().size()-1) {
                        kk = kk / 2;
                        kk = kk / party.getParticipant().size();

                        if (kk <= 0.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_empty);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 1.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_half);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 1.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_empty);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 2.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_half);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 2.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 3.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_half);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 3.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_empty);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 4.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_half);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 4.5) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_empty);
                        } else if (kk < 5.0) {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_half);
                        } else {
                            bottleFirst.setImageResource(R.drawable.ic_bottle_full);
                            bottleSecond.setImageResource(R.drawable.ic_bottle_full);
                            bottleThird.setImageResource(R.drawable.ic_bottle_full);
                            bottleFourth.setImageResource(R.drawable.ic_bottle_full);
                            bottleFifth.setImageResource(R.drawable.ic_bottle_full);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
        Adapter adapter = new Adapter(HostRoomDetail.this, testData);
        recyclerView.setAdapter(adapter);


//        recyclerView.setHasFixedSize(true);

        storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com/");
// Create a reference with an initial file path and name
//        StorageReference pathReference = storageRef.child(“images/profile.jpg”);
// Create a reference to a file from a Google Cloud Storage URI
        StorageReference gsReference = storage.getReferenceFromUrl("gs://kookje-namnyeo-android.appspot.com/images/profile.jpg");
// Create a reference from an HTTPS URL
// Note that in the URL, characters are URL escaped!
//        StorageReference httpsReference = storage.getReferenceFromUrl(“https://firebasestorage.googleapis.com/b/kookje-namnyeo-android.appspot.com/o/images%profile.jpg”);



//        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        final String userId = user.getUid();
//


        /**
         //        //TODO : user 가져와서
         //        if (null != adapter) {
         //            adapter.setDatas(여기다가 리스트 넣으면됨 List<USER>);
         }
         */
//        mDatabase.getReference("users").child(userId).addListenerForSingleValueEvent(
//                new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        // Get user value
//                        User user = dataSnapshot.getValue(User.class);
//                        String nickName = user.getNickname();
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        Log.w("test", "getUser:onCancelled", databaseError.toException());
//                    }
//                });

        findViewById(R.id.arrow_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext()
                        , PartyListActivity.class);
                startActivity(intent);

            }
        });

        findViewById(R.id.imageView5).setOnClickListener(new View.OnClickListener(){

            int count = 0;

            @Override
            public void onClick(View v) {
                count += 1;
                if (count == 1) {
                    findViewById(R.id.behind_button).setVisibility(View.VISIBLE);

                } else {
                    count = 0;
                    findViewById(R.id.behind_button).setVisibility(View.GONE);
                }


            }
        });

        findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext()
                        , RoomEditActivity.class);
                intent.putExtra("party", party);
                startActivity(intent);

            }
        });

        alert_confirm = new AlertDialog.Builder(HostRoomDetail.this);


        findViewById(R.id.behind_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alert_confirm.setMessage("정말로 삭제하시겠습니까?").setCancelable(false).setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDatabase.getReference("parties").child(party.getId()).setValue(null);
                                finish();
                                Intent intent = new Intent(getApplicationContext()
                                        , PartyListActivity.class);
                                startActivity(intent);

                            }
                        }).setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();

            }
        });

        TextView hashTagsTextFirst = (TextView) findViewById(R.id.hashTagsTextFirst);
//        hashTagsTextSecond = (TextView) findViewById(R.id.hashTagsTextSecond);
//        hashTagsTextThird = (TextView) findViewById(R.id.hashTagsTextThird);
        TextView dateText = (TextView) findViewById(R.id.dateText);
        TextView titleText = (TextView) findViewById(R.id.textTitle);
        TextView participateNum = (TextView) findViewById(R.id.maxText);
        ImageView userPhoto = (ImageView) findViewById(R.id.userphoto);


        String titleString = party.getTitle();
        titleText.setText(titleString);

        String hashTagsStringFirst = party.getHashtags();
        hashTagsTextFirst.setText(hashTagsStringFirst);

        Long millisecond = party.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd a hh:mm");
        String dateLong = formatter.format(new Date(millisecond));
        dateText.setText(dateLong);
        Button button3 = (Button) findViewById(R.id.onclosebuttonhost);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout r2 = (RelativeLayout) findViewById(R.id.relativelayouts2);
                r2.setVisibility(View.GONE);
            }
        });

        String merge = party.getParticipant().size() + "/" + party.getMax();
        participateNum.setText(merge);
        if (party.getMax() == 1) {
            participateNum.setText(party.getParticipant().size() + "/∞");
        }

        StorageReference pathReference = storageRef.child("images").child("profile").child(party.getHostId());

        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(pathReference)
                .into(userPhoto);




        dia_name = (TextView) findViewById(R.id.dia_name);
        dia_gender = (TextView) findViewById(R.id.dia_gender);
        dia_country = (TextView) findViewById(R.id.dia_country);
        dia_department = (TextView) findViewById(R.id.dia_department);




    }
    public void setViz2(int pos, User user) {

        String p = party.getParticipant().get(pos).toString();

        dia_name.setText(user.getNickname());
        dia_gender.setText(user.getGender());
        dia_country.setText(user.getCountry());
        dia_department.setText(user.getDepartment());


        RelativeLayout r2 = (RelativeLayout) findViewById(R.id.relativelayouts2);

        r2.setVisibility(View.VISIBLE);

//        r.bringToFront();


    }




}
