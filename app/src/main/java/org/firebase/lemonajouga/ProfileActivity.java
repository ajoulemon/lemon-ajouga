package org.firebase.lemonajouga;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static org.firebase.lemonajouga.R.id.nickName;

public class ProfileActivity extends AppCompatActivity {
//    private EditText EditnickName;
//    private Spinner spinCountry;
    private Button prevButton;
    private Button nextButton;
    private DatabaseReference mDatabase;
//    private EditText nickName;
    private RadioButton man;
    private RadioButton woman;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private String gender;

    AlertDialog.Builder alert_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        prevButton = (Button) findViewById(R.id.prevButton);
        nextButton = (Button) findViewById(R.id.nextButton);
        man = (RadioButton) findViewById(R.id.man);
        woman = (RadioButton) findViewById(R.id.woman);
        radioGroup = (RadioGroup) findViewById(R.id.radio);



        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext()
                        , PhotoActivity.class);
                startActivity(intent);
            }
        });

        final Spinner spinCountry = (Spinner) findViewById(R.id.spinCountry);
        final EditText EditnickName = (EditText) findViewById(nickName);

        spinCountry.setSelection(195);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                String uid = user.getUid();

                String nickname = EditnickName.getText().toString();
                String country = spinCountry.getSelectedItem().toString();
                String photo = getIntent().getStringExtra("photo");

                if (isValidate(nickname)) {
                    Intent intent = new Intent(getApplicationContext()
                            , InfoActivity.class);

                    intent.putExtra("uid", uid);
                    intent.putExtra("nickname", nickname);
                    intent.putExtra("country", country);
                    intent.putExtra("gender", gender);
                    intent.putExtra("photo", photo);

                    startActivity(intent);

                } else {
                    alert_confirm = new AlertDialog.Builder(ProfileActivity.this);
                    alert_confirm.setMessage("정보를 모두 입력해주세요").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();

                    // alert 띄우기
                }


            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int childCount = group.getChildCount();

                gender = null;

                for (int x = 0; x < childCount; x++) {
                    RadioButton btn = (RadioButton) group.getChildAt(x);


                    if(btn.getId()==R.id.man){
                        btn.setText("M");
                    }else{
                        btn.setText("F");
                    }
                    if (btn.getId() == checkedId) {

                        gender= btn.getText().toString();// here gender will contain M or F.

                    }

                }
            }
        });


        //   ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.countryArray));
//
//        final EditText editTitle = (EditText) findViewById(R.id.editText);
//        final EditText editTag = (EditText) findViewById(R.id.editTag);




    }


    public boolean isValidate(String s) {
        boolean check = s.length() != 0;

        return check;
    }



//    public void addListenerOnButton() {
//
//
//
//
//
//        nextButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                // get selected radio button from radioGroup
//                int selectedId = radioGroup.getCheckedRadioButtonId();
//
//                // find the radiobutton by returned id
//                radioButton = (RadioButton) findViewById(selectedId);
//
//
//            }
//        });
//    }
}


//    }

//    ChildEventListener childEventListener = new ChildEventListener() {
//        @Override
//        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
//            Log.d("test", "onChildAdded:" + dataSnapshot.getKey());
//
//            // A new comment has been added, add it to the displayed list
//            Comment comment = dataSnapshot.getValue(Comment.class);
//
//            // ...
//        }
//    };






