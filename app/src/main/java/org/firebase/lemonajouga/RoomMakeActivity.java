package org.firebase.lemonajouga;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import org.firebase.lemonajouga.models.Party;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.firebase.lemonajouga.R.id.Create_btn;

public class RoomMakeActivity extends AppCompatActivity {

    private static final String TAG = "RoomMake";

    AlertDialog.Builder alert_confirm;

    private FirebaseDatabase mDatabase;
    FirebaseUser user;

    GregorianCalendar calendar;

    private HashTagHelper mTextHashTagHelper;
    private TextView mHashTagText;

    String max_spin;
    String pid;
    String title;
    String hashtags;
    String hostId;
    String location;

    Long date;
    int max;

    public String test;

//    long startTime;

    int year, month, day, hour, minute;
    int real_year, real_month, real_day, real_hour, real_minute;

    String am_pm;

    String userId;

    String nm;
    String cnt;

    Party parties;
    List<String> partyList = new ArrayList<>();
    List<String> party;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_make);
        FirebaseApp.initializeApp(this);

        //FireBase
        mDatabase = FirebaseDatabase.getInstance();

        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        alert_confirm = new AlertDialog.Builder(RoomMakeActivity.this);

        mDatabase.getReference().child("users").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                HashMap u = (HashMap) dataSnapshot.getValue();

                if (null != u.get("parties")) {
                    partyList.addAll((Collection<? extends String>) u.get("parties"));
                }

                nm = u.get("nickname").toString();
                cnt = u.get("country").toString();

//                Log.d("FIREBASE","ArrayList "+ nmList.toString() + cntList.toString());
//
//
//                storageContainer(nmList, cntList);
//

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        // Hash Tag
        mHashTagText = (TextView) findViewById(R.id.editTag);
        mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.colorPrimary),
                new HashTagHelper.OnHashTagClickListener() {
                    @Override
                    public void onHashTagClicked(String hashTag) {
                        Log.d(TAG, hashTag);
                    }
                });

        // pass a TextView or any descendant of it (incliding EditText) here.
        // Hash tags that are in the text will be hightlighed with a color passed to HasTagHelper
        mTextHashTagHelper.handle(mHashTagText);

        test = mHashTagText.getText().toString();
        Log.d(TAG, test);

        //Picker

        calendar = (GregorianCalendar) new GregorianCalendar().getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        findViewById(R.id.Date_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(RoomMakeActivity.this, dateSetListener, year, month, day).show();

            }
        });

        findViewById(R.id.Time_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(RoomMakeActivity.this, timeSetListener, hour, minute, false).show();
            }
        });


        final EditText editTitle = (EditText) findViewById(R.id.editText);
        final Spinner spin_loc = (Spinner) findViewById(R.id.spinner_location);
        final Spinner spin_max = (Spinner) findViewById(R.id.spinner_max);
        final EditText editTag = (EditText) findViewById(R.id.editTag);

        findViewById(Create_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pid = mDatabase.getReference().push().getKey();

                title = editTitle.getText().toString();
                hashtags = editTag.getText().toString();





                hostId = userId;
                calendar.set(real_year, real_month, real_day,real_hour,real_minute, 0);
                date = calendar.getTimeInMillis();

                max_spin = spin_max.getSelectedItem().toString();

                location = spin_loc.getSelectedItem().toString();

//                Intent intentUser = new Intent(getApplicationContext()
//                        , UserRoomDetail.class);
//
//
//                Intent intentHost = new Intent(getApplicationContext()
//                        , HostRoomDetail.class);
//
//                intentUser.putExtra("placeUser", place);
//
//                intentHost.putExtra("placeHost", place);


                if (max_spin.equals("2명")) {
                    max = 2;
                } else if (max_spin.equals("3명")) {
                    max = 3;
                } else if (max_spin.equals("4명")) {
                    max = 4;
                } else if (max_spin.equals("5명")) {
                    max = 5;
                } else if (max_spin.equals("6명")) {
                    max = 6;
                } else if (max_spin.equals("7명")) {
                    max = 7;
                } else if (max_spin.equals("8명+")) {
                    max = 1;
                }

                if (isValidate(title) & isValidate(hashtags) & isValidate(date.toString())) {
                    alert_confirm.setMessage("이 정보로 방을 생성하시겠습니까?").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    parties = new Party(pid, title, hashtags , hostId, date, max, location);
                                    List<String> participant = new ArrayList<>();

                                    participant.add(hostId);
                                    parties.setParticipant(participant);



                                    parties.setHostNickname(nm);
                                    parties.setHostCountry(cnt);
//                                    parties.setLocation(location);

                                    // parties에 정보 추가
                                    mDatabase.getReference().child("parties").child(pid).setValue(parties.toMap());


                                    partyList.add(pid);
//                                   result.put("hostCountry", hostCountry);

                                    mDatabase.getReference().child("users").child(hostId).child("parties").setValue(partyList);

                                    Toast.makeText(RoomMakeActivity.this, "성공적으로 방이 생성되었습니다.", Toast.LENGTH_SHORT).show();
                                    finish();

                                }
                            }).setNegativeButton("취소",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();

                } else {
                    alert_confirm.setMessage("정보를 모두 입력해주세요").setCancelable(false).setPositiveButton("확인",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                    AlertDialog alert = alert_confirm.create();
                    alert.show();
                }


//                isValidate(title) || isValidate(hashtags);


//                List<String> particiPants = new ArrayList<>();
//                particiPants.add("user1");
//                particiPants.add("user2");

            }
        });

        findViewById(R.id.goback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub

            real_year = year;
            real_month = monthOfYear;
            real_day = dayOfMonth;

            String msg = String.format("%d-%d-%d", year,monthOfYear+1, dayOfMonth);
            Toast.makeText(RoomMakeActivity.this, msg, Toast.LENGTH_SHORT).show();

            TextView DTxt = (TextView) findViewById(R.id.Date_text);
            DTxt.setText(msg);

        }

    };

    TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // TODO Auto-generated method stub

            real_hour = hourOfDay;
            real_minute = minute;
            if (real_hour >= 12 & real_hour < 24) {
                am_pm = "PM";
                String msg = String.format("%s %d:%d", am_pm, hourOfDay-12, minute);
                Toast.makeText(RoomMakeActivity.this, msg, Toast.LENGTH_SHORT).show();

                TextView TTxt = (TextView) findViewById(R.id.Time_text);
                TTxt.setText(msg);

            } else {
                am_pm = "AM";
                String msg = String.format("%s %d:%d", am_pm, hourOfDay, minute);
                Toast.makeText(RoomMakeActivity.this, msg, Toast.LENGTH_SHORT).show();

                TextView TTxt = (TextView) findViewById(R.id.Time_text);
                TTxt.setText(msg);

            }
        }

    };

    public boolean isValidate(String s) {
        boolean check = s.length() != 0;

        return check;
    }




//    public void onButtonCreate(String userId, String name, String email) {
//        Users user = new Users(name, email);
//        mDatabase.child("users").child(userId).setValue(user);
//
//        Log.d(TAG, test);
//    }
}